package uj.java.pwj2019.map2d;

import java.util.Objects;

public class Pair<R, C> {
    private R row;
    private C col;

    public R getRow() {
        return row;
    }

    public void setRow(R row) {
        this.row = row;
    }

    public C getCol() {
        return col;
    }

    public void setCol(C col) {
        this.col = col;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pair<?, ?> pair = (Pair<?, ?>) o;
        return row.equals(pair.row) &&
                col.equals(pair.col);
    }

    @Override
    public int hashCode() {
        return Objects.hash(row, col);
    }

    public Pair(R row, C col) {
        this.row = row;
        this.col = col;
    }
}
