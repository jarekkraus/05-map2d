package uj.java.pwj2019.map2d;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Mapa2D<R, C, V> implements Map2D<R, C, V>{

    HashMap<Pair<R, C>, V> mapa = new HashMap<>();

    public Mapa2D(HashMap<Pair<R, C>, V> outputValues) {
        outputValues.forEach((key, val) -> {
            mapa.put(new Pair<>(key.getRow(), key.getCol()), val);
        });
    }

    public Mapa2D() {
        mapa = new HashMap<>();
    }

    @Override
    public V put(R rowKey, C columnKey, V value) {
        if (rowKey == null || columnKey == null) {
            throw new NullPointerException();
        }
        Pair<R, C> key = new Pair<>(rowKey, columnKey);
        if (mapa.containsKey(key)) {
            V prevValue = mapa.get(key);
            mapa.put(key,value);
            return prevValue;
        } else {
            mapa.put(key, value);
            return null;
        }
    }

    @Override
    public V get(R rowKey, C columnKey) {
        Pair<R, C> key = new Pair<>(rowKey, columnKey);
        if (mapa.containsKey(key)) {
            return mapa.get(key);
        } else {
            return null;
        }
    }

    @Override
    public V getOrDefault(R rowKey, C columnKey, V defaultValue) {
        Pair<R, C> key = new Pair<>(rowKey, columnKey);
        if (mapa.containsKey(key)) {
            return mapa.get(key);
        } else {
            return defaultValue;
        }
    }

    @Override
    public V remove(R rowKey, C columnKey) {
        Pair<R, C> key = new Pair<>(rowKey, columnKey);
        if (mapa.containsKey(key)) {
            V prevValue = mapa.get(key);
            mapa.remove(key);
            return prevValue;
        } else {
            return null;
        }
    }

    @Override
    public boolean isEmpty() {
        return mapa.isEmpty();
    }

    @Override
    public boolean nonEmpty() {
        return mapa.size() > 0;
    }

    @Override
    public int size() {
        return mapa.size();
    }

    @Override
    public void clear() {
        mapa.clear();
    }

    @Override
    public Map<C, V> rowView(R rowKey) {
        return mapa
                .keySet()
                .stream()
                .filter(pair -> pair.getRow() == rowKey)
                .collect(Collectors.toUnmodifiableMap(pair -> pair.getCol(), pair -> mapa.get(pair)));
    }

    @Override
    public Map<R, V> columnView(C columnKey) {
        return mapa
                .keySet()
                .stream()
                .filter(pair -> pair.getCol() == columnKey)
                .collect(Collectors.toUnmodifiableMap(pair -> pair.getRow(), pair -> mapa.get(pair)));
    }

    @Override
    public boolean hasValue(V value) {
        return mapa.containsValue(value);
    }

    @Override
    public boolean hasKey(R rowKey, C columnKey) {
        return mapa.containsKey(new Pair(rowKey, columnKey));
    }

    @Override
    public boolean hasRow(R rowKey) {
        return mapa
                .keySet()
                .stream()
                .anyMatch(pair -> pair.getRow().equals(rowKey));
    }

    @Override
    public boolean hasColumn(C columnKey) {
        return mapa
                .keySet()
                .stream()
                .anyMatch(pair -> pair.getCol().equals(columnKey));
    }

    @Override
    public Map<R, Map<C, V>> rowMapView() {
        Map<R, Map<C, V>> result = new HashMap<>();
        mapa.forEach((key, val) -> {
            if (!result.containsKey(key.getRow())) {
                result.put(key.getRow(), new HashMap<>());
            }
            result.get(key.getRow()).put(key.getCol(), val);
        });
        return result;
    }

    @Override
    public Map<C, Map<R, V>> columnMapView() {
        Map<C, Map<R, V>> result = new HashMap<>();
        mapa.forEach((key, val) -> {
            if (!result.containsKey(key.getCol())) {
                result.put(key.getCol(), new HashMap<>());
            }
            result.get(key.getCol()).put(key.getRow(), val);
        });
        return result;
    }

    @Override
    public Map2D<R, C, V> fillMapFromRow(Map<? super C, ? super V> target, R rowKey) {
        mapa.keySet().stream().forEach(pair -> {
            if (pair.getRow().equals(rowKey)) {
                target.put(pair.getCol(), mapa.get(pair));
            }
        });
        return this;
    }

    @Override
    public Map2D<R, C, V> fillMapFromColumn(Map<? super R, ? super V> target, C columnKey) {
        mapa.keySet().stream().forEach(pair -> {
            if (pair.getCol().equals(columnKey)) {
                target.put(pair.getRow(), mapa.get(pair));
            }
        });
        return this;
    }

    @Override
    public Map2D<R, C, V> putAll(Map2D<? extends R, ? extends C, ? extends V> source) {
        ((Mapa2D) source).mapa.forEach((key, val) -> {
            mapa.put((Pair)key, (V)val);
        });
        return this;
    }

    @Override
    public Map2D<R, C, V> putAllToRow(Map<? extends C, ? extends V> source, R rowKey) {
        source.forEach((key, val) -> {
            mapa.put(new Pair(rowKey, key), val);
        });
        return this;
    }

    @Override
    public Map2D<R, C, V> putAllToColumn(Map<? extends R, ? extends V> source, C columnKey) {
        source.forEach((key, val) -> {
            mapa.put(new Pair(key, columnKey), val);
        });
        return this;
    }

    @Override
    public <R2, C2, V2> Map2D<R2, C2, V2> copyWithConversion(Function<? super R, ? extends R2> rowFunction, Function<? super C, ? extends C2> columnFunction, Function<? super V, ? extends V2> valueFunction) {
        HashMap<Pair<R2, C2>, V2> outputValues = new HashMap<>();
        mapa
                .keySet()
                .stream()
                .forEach(pair -> outputValues.put(new Pair<>(rowFunction.apply(pair.getRow()), columnFunction.apply(pair.getCol())),
                                valueFunction.apply(mapa.get(pair))));
        return new Mapa2D<>(outputValues);
    }
}
